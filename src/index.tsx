import ReactDOM from "react-dom/client";
import { BrowserRouter } from "react-router-dom";
import { Provider } from "react-redux";
import createStore from "./redux/store";
import App from "./App";

const appEl = document.getElementById("app") as HTMLDivElement;
const root = ReactDOM.createRoot(appEl);
const store = createStore();
root.render(
  <Provider store={store}>
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </Provider>
);
