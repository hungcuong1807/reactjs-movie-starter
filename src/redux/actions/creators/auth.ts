import * as ActionTypes from "../types";
import { TCreateUser, TErrorResponse, TLoginUser } from "../../../api/types";

export const fetchRegister = (
  user: TCreateUser,
  callback?: (error?: TErrorResponse) => void
) => ({
  type: ActionTypes.FETCH_SIGNUP,
  payload: user,
  callback,
});

export const fetchLoginUser = (
  user: TLoginUser,
  callback?: (error?: TErrorResponse) => void
) => ({
  type: ActionTypes.FETCH_SIGNIN,
  payload: user,
  callback,
});

export const fetchLogoutUser = (
  callback?: (error?: TErrorResponse) => void
) => ({
  type: ActionTypes.FETCH_LOGOUT_USER,
  payload: {},
  callback,
});

export const fetchUserProfile = (
  callback?: (error?: TErrorResponse) => void
) => ({
  type: ActionTypes.FETCH_USER_PROFILE,
  payload: {},
  callback,
});
