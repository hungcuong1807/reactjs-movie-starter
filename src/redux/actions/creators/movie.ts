import { TErrorResponse, TShareMovie } from "../../../api/types";
import * as ActionTypes from "../types";

export const fetchMovieList = (
  page: number,
  limit: number,
  search_text: string,
  callback?: (error?: TErrorResponse) => void
) => ({
  type: ActionTypes.FETCH_MOVIE_LIST,
  payload: { page, limit, searchText: search_text },
  callback,
});

export const fetchShareMovie = (
  movie: TShareMovie,
  callback?: (error?: TErrorResponse) => void
) => ({
  type: ActionTypes.FETCH_SHARE_MOVIE,
  payload: movie,
  callback,
});
