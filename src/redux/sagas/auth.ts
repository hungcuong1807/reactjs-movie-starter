import { AnyAction } from "redux";
import { call, put, takeLatest } from "redux-saga/effects";
import * as ActionTypes from "../actions/types";
import { TAuthResponse, TErrorResponse } from "../../api/types";
import * as API from "../../api";

function* fetchSignIn(action: AnyAction) {
  let error: TErrorResponse = { statusCode: 0, message: "" };

  try {
    const user = action.payload;
    const response: TAuthResponse | TErrorResponse = yield call(() =>
      API.login(user)
    );

    if ((response as TErrorResponse).statusCode) {
      error = response as TErrorResponse;
    } else {
      yield call(() =>
        localStorage.setItem("token", (response as TAuthResponse).access_token)
      );
      yield put({
        type: ActionTypes.RESOLVE_SIGNIN,
        payload: (response as TAuthResponse).user,
      });
    }
  } catch (e) {
    yield put({
      type: ActionTypes.RESOLVE_SIGNIN,
      payload: { id: "", email: "" },
    });
  } finally {
    if (action.callback) {
      action.callback(error);
    }
  }
}

function* fetchSignUp(action: AnyAction) {
  let error: TErrorResponse = { statusCode: 0, message: "" };
  try {
    const user = action.payload;
    const response: TAuthResponse | TErrorResponse = yield call(() =>
      API.register(user)
    );
    if ((response as TErrorResponse).statusCode) {
      error = response as TErrorResponse;
    } else {
      yield call(() =>
        localStorage.setItem("token", (response as TAuthResponse).access_token)
      );

      yield put({
        type: ActionTypes.RESOLVE_SIGNUP,
        payload: (response as TAuthResponse).user,
      });
    }
  } catch (e) {
    yield put({
      type: ActionTypes.RESOLVE_SIGNUP,
      payload: { id: "", email: "" },
    });
  } finally {
    if (action.callback) {
      action.callback(error);
    }
  }
}

function* fetchUserProfile(action: AnyAction) {
  let error: TErrorResponse = { statusCode: 0, message: "" };
  try {
    const response: TAuthResponse | TErrorResponse = yield call(() =>
      API.getUserProfile()
    );
    if ((response as TErrorResponse).statusCode) {
      error = response as TErrorResponse;
    } else {
      yield put({
        type: ActionTypes.RESOLVE_USER_PROFILE,
        payload: response as TAuthResponse,
      });
    }
  } catch (e) {
    yield put({
      type: ActionTypes.RESOLVE_USER_PROFILE,
      payload: { id: "", email: "" },
    });
  } finally {
    if (action.callback) {
      action.callback(error);
    }
  }
}

function* fetchLogout(action: AnyAction) {
  let error: TErrorResponse = { statusCode: 0, message: "" };
  try {
    const response: TAuthResponse | TErrorResponse = yield call(() =>
      API.logout()
    );
    if ((response as TErrorResponse).statusCode) {
      error = response as TErrorResponse;
    } else {
      yield call(() => localStorage.removeItem("token"));

      yield put({
        type: ActionTypes.RESOLVE_LOGOUT_USER,
        payload: { id: "", email: "" },
      });
    }
  } catch (e) {
    yield put({
      type: ActionTypes.RESOLVE_LOGOUT_USER,
      payload: { id: "", email: "" },
    });
  } finally {
    if (action.callback) {
      action.callback(error);
    }
  }
}

export function* authSagaIterator() {
  yield takeLatest(ActionTypes.FETCH_SIGNIN, fetchSignIn);
  yield takeLatest(ActionTypes.FETCH_SIGNUP, fetchSignUp);
  yield takeLatest(ActionTypes.FETCH_USER_PROFILE, fetchUserProfile);
  yield takeLatest(ActionTypes.FETCH_LOGOUT_USER, fetchLogout);
}
