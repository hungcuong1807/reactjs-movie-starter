import { all, fork } from "redux-saga/effects";

// eslint-disable-next-line import/no-named-as-default
import { authSagaIterator } from "./auth";
import { movieSagaIterator } from "./movie";

export default function* rootSaga() {
  yield all([fork(authSagaIterator), fork(movieSagaIterator)]);
}
