import { AnyAction } from "redux";
import { call, put, takeLatest } from "redux-saga/effects";

import * as ActionTypes from "../actions/types";
import { TErrorResponse, TMovie, TMovieListResponse } from "../../api/types";

import * as API from "../../api";

function* fetchMovieList(action: AnyAction) {
  let error: TErrorResponse = { statusCode: 0, message: "" };
  try {
    const { page, limit, searchText } = action.payload;
    const response: TMovieListResponse | TErrorResponse = yield call(() =>
      API.getListMovie(page, limit, searchText)
    );
    if ((response as TErrorResponse).statusCode) {
      error = response as TErrorResponse;
    } else {
      yield put({
        type: ActionTypes.RESOLVE_MOVIE_LIST,
        payload: {
          movies: (response as TMovieListResponse).items,
          page: (response as TMovieListResponse).pagination.current_page,
          total_pages: (response as TMovieListResponse).pagination.total_pages,
        },
      });
    }
  } catch (e) {
    yield put({
      type: ActionTypes.RESOLVE_MOVIE_LIST,
      payload: {
        movies: [],
        page: 1,
        total_pages: 1,
      },
    });
  } finally {
    if (action.callback) {
      action.callback(error);
    }
  }
}

function* fetchShareMovie(action: AnyAction) {
  let error: TErrorResponse = { statusCode: 0, message: "" };

  try {
    const movie = action.payload;
    const response: TErrorResponse | TMovie = yield call(() =>
      API.shareMovie(movie)
    );
    if ((response as TErrorResponse).statusCode) {
      error = response as TErrorResponse;
    }
  } catch (e) {
    // eslint-disable-next-line no-alert
    alert(e);
  } finally {
    if (action.callback) {
      action.callback(error);
    }
  }
}

export function* movieSagaIterator() {
  yield takeLatest(ActionTypes.FETCH_MOVIE_LIST, fetchMovieList);
  yield takeLatest(ActionTypes.FETCH_SHARE_MOVIE, fetchShareMovie);
}
