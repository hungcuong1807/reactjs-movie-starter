import { applyMiddleware, compose, createStore } from "redux";
import createSagaMiddleware from "redux-saga";

import rootReducers from "./reducers";
import rootSaga from "./sagas";

const sagaMiddleware = createSagaMiddleware();

const middleware = [sagaMiddleware];
const composeEnhancers = compose;

const configureStore = () => {
  const store = createStore(
    rootReducers,
    // preloadedState,
    composeEnhancers(applyMiddleware(...middleware))
  );

  // if (process.env.NODE_ENV === "development") {
  //   if (module.hot) {
  //     module.hot.accept('./reducers', () => {
  //       store.replaceReducer(rootReducers)
  //     })
  //   }
  // }

  // the run() function must be called after the store is created
  sagaMiddleware.run(rootSaga);

  return store;
};

export default configureStore;
