import { AnyAction } from "redux";

import * as ActionTypes from "../actions/types";
import { Tuser } from "../../api/types";

export type TAuthState = {
  user: Tuser;
};

const initialState: TAuthState = {
  user: { id: "", email: "" },
};

export default (state = initialState, action: AnyAction) => {
  switch (action.type) {
    case ActionTypes.RESOLVE_SIGNUP:
      return {
        ...state,
        user: action.payload,
      };
    case ActionTypes.RESOLVE_SIGNIN:
      return {
        ...state,
        user: action.payload,
      };
    case ActionTypes.RESOLVE_USER_PROFILE:
      return {
        ...state,
        user: action.payload,
      };
    case ActionTypes.RESOLVE_LOGOUT_USER:
      return {
        ...state,
        user: { id: "", email: "" },
      };
    default:
      return state;
  }
};
