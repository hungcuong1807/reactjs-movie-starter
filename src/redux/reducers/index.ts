import { combineReducers } from "redux";
import authReducer, { TAuthState } from "./auth";
import movieReducer, { TMovieState } from "./movie";

// @flow
export type TStateType = {
  auth: TAuthState;
  movie: TMovieState;
};

export default combineReducers<TStateType>({
  auth: authReducer,
  movie: movieReducer,
});
