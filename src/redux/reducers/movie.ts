import { AnyAction } from "redux";

import * as ActionTypes from "../actions/types";
import { TMovie } from "../../api/types";

export type TMovieState = {
  current_page: number;
  total_pages: number;
  movies: TMovie[];
};

const initialState: TMovieState = {
  movies: [],
  current_page: 1,
  total_pages: 1,
};

export default (state = initialState, action: AnyAction) => {
  switch (action.type) {
    case ActionTypes.RESOLVE_MOVIE_LIST:
      return {
        ...state,
        current_page: action.payload.page,
        total_pages: action.payload.total_pages,
        movies:
          action.payload.page <= 1 ?
            action.payload.movies :
            [...state.movies, ...action.payload.movies],
      };
    default:
      return state;
  }
};
