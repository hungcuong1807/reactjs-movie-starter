import { render, fireEvent } from "@testing-library/react";
import "jest-environment-jsdom";
import { Provider } from "react-redux";
import { MemoryRouter } from "react-router-dom";

import createStore from "../redux/store";
import Register from "../pages/sign/register";
import "@testing-library/jest-dom";

describe("Register user", () => {
  let store: any;
  beforeAll(() => {
    store = createStore();
  });
  it("Register user", async () => {
    const screen = render(
      <Provider store={store}>
        <MemoryRouter>
          <Register />
        </MemoryRouter>
      </Provider>
    );
    const emailInput = screen.container.querySelector(`input[name="email"]`);
    const passwordInput = screen.container.querySelector(
      `input[name="password"]`
    );
    if (emailInput && passwordInput) {
      fireEvent.change(emailInput, {
        target: { value: "cuongnh1807@gmail.com" },
      });

      fireEvent.change(passwordInput, {
        target: { value: "Test1234" },
      });
    }

    fireEvent.click(screen.getByText("Sign Up"));
    expect(emailInput).not.toBeInTheDocument();
  });
});
