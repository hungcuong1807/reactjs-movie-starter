import { Route, Routes, Navigate } from 'react-router-dom';
import Layout from './components/Layout';
import HomePage from './pages/home';
import Login from './pages/sign/login';
import Register from './pages/sign/register';
// import ProfilePage from './pages/profile';
import AuthGuardedRoute from './pages/AuthGuardedRoute';

import './App.css';
import ShareFormPage from './pages/share/share-movie';

const App = () => (
  <Routes>
    <Route element={<Layout />}>
      <Route path='/' element={<HomePage />} />
      {/* <Route path='products'>
        <Route index element={<ProductsPage />} />
        <Route path=':id' element={<ProductDetails />} />
        <Route path='categories'>
          <Route index element={<CategoriesPage />} />
          <Route path=':category' element={<CategoryDetails />} />
        </Route>
        <Route path='search' element={<SearchPage />} />
      </Route> */}
      <Route element={<AuthGuardedRoute />}>
        <Route path='share' element={<ShareFormPage />} />
      </Route>
    </Route>

    <Route path='login' element={<Login />} />
    <Route path='register' element={<Register />} />
    <Route path='*' element={<Navigate to='/' />} />
  </Routes>
);

export default App;
