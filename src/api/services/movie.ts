import { TShareMovie } from "../types";
import { request } from "..";

export const getListMovie = (
  page: number,
  limit: number,
  search_text?: string
) =>
  request("/movies", {
    method: "GET",
    body: JSON.stringify({
      search_text,
      page,
      limit,
      order_by: "created_at",
      sort_by: "desc",
    }),
  });

export const shareMovie = (movie: TShareMovie) =>
  request("/movies/share", { method: "POST", body: movie });
