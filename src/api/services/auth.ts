import { TCreateUser, TLoginUser } from "../types";
import request from "../helpers/https";

export const register = (user: TCreateUser) =>
  request("/auth/signup", { method: "POST", body: user });

export const login = (user: TLoginUser) =>
  request("/auth/signin", { method: "POST", body: user });

export const logout = () => request("/auth/logout", { method: "DELETE" });

export const getUserProfile = () => request("/auth/profile", { method: "GET" });
