export type TCreateUser = {
  email: string;
  password: string;
};

export type TLoginUser = {
  email: string;
  password: string;
};

export type TShareMovie = {
  title?: string | null;
  description?: string | null;
  video_url: string;
};

export type TMovie = {
  id?: string;
  title?: string | null;
  description?: string | null;
  video_url: string;
  shared_user_email: string;
};

export type Tuser = {
  id: string;
  email: string;
};

export type TAuthResponse = {
  access_token: string;
  expires_in: number;
  token_type: string;
  user: Tuser;
};

export type TMovieListResponse = {
  items: TMovie[];
  pagination: {
    current_page: number;
    total_pages: number;
    per_page: number;
  };
};

export type TErrorResponse = {
  statusCode: number;
  message: string;
  code?: number;
};
