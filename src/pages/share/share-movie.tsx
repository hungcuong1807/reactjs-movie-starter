import { useState, FormEvent } from "react";
import { useNavigate } from "react-router-dom";
import "./share.css";
import { useDispatch } from "react-redux";
import { TErrorResponse, TShareMovie } from "../../api/types";

import { fetchShareMovie } from "../../redux/actions/creators";
import Loading from "../../components/Loading";

const ShareFormPage = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const [inputError, setInputError] = useState("");
  const [isLoading, setLoading] = useState(false);
  const [inputSuccess, setInputSuccess] = useState("");

  const isValidForm = (formData: TShareMovie) => {
    // eslint-disable-next-line @typescript-eslint/naming-convention
    const { video_url } = formData;
    if (!video_url.length) {
      setInputError("Please fill videoURL");
      return false;
    }

    return true;
  };
  const formSubmitHandler = async (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    const formData = new FormData(e.currentTarget) as Iterable<[TShareMovie]>;
    const entries: TShareMovie = Object.fromEntries(formData);

    // eslint-disable-next-line no-useless-return
    if (!isValidForm(entries)) return;
    setLoading(true);
    dispatch(
      fetchShareMovie(entries, (error?: TErrorResponse) => {
        setLoading(false);
        if (error && error.message) {
          setInputError(error.message || "Something went wrong");
        } else {
          setInputSuccess("Share video success");
          setTimeout(() => {
            navigate("/");
          }, 200);
        }
      })
    );
  };

  return isLoading ? (
    <Loading />
  ) : (
    <div className='share-page'>
      <h2 style={{ color: "black" }}>Share Movie</h2>
      <form action='/share' onSubmit={formSubmitHandler}>
        <div
          className={inputError.length ? "form-item error-item" : "form-item"}
        >
          <div className='form-field'>
            <input type='text' name='video_url' placeholder='Video:' />
          </div>
          <div className='form-field' style={{ marginTop: 20 }}>
            <input type='text' name='title' placeholder='Title:' />
          </div>
        </div>
        <div
          className={inputError.length ? "form-item error-item" : "form-item"}
        >
          <div className='share-filed-area'>
            <textarea
              style={{ height: 100 }}
              name='description'
              placeholder='Description:'
            />
          </div>

          <div className='form-error'>{inputError}</div>
          <div className='form-success'>{inputSuccess}</div>
        </div>
        <div className='form-submit'>
          <button type='submit'>Share video</button>
        </div>
      </form>
    </div>
  );
};

export default ShareFormPage;
