// import { useAuthUserSelector } from '../../redux/auth/auth.slice';
// import { useGetCategoriesQuery } from '../../redux/products/product.api';
// import { useProductsSelector } from '../../redux/products/product.slice';
// import CategoriesList from '../categories/components/CategoriesList';
// import ProductsList from '../products/components/ProductsList';

import { useCallback, useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import Loading from "src/components/Loading";
import { fetchMovieList } from "../../redux/actions/creators";
import { TStateType } from "../../redux/reducers";
import { TMovieState } from "../../redux/reducers/movie";
import MovieElement from "./movies";
import { TMovie } from "../../api/types";

const HomePage = () => {
  const dispatch = useDispatch();
  const movieState: TMovieState = useSelector(
    (state: TStateType) => state.movie
  );
  const [page, setPage] = useState(1);
  const [initLoading, setInitLoading] = useState<boolean>(false);

  useEffect(() => {
    if (page === 1) {
      setInitLoading(true);
    }

    dispatch(
      fetchMovieList(page, 10, "", () => {
        setInitLoading(false);
      })
    );
  }, []);

  const loadMore = useCallback(() => {
    if (page < movieState.total_pages) {
      dispatch(
        fetchMovieList(page + 1, 10, "", () => {
          const newPage = page + 1;
          setPage(newPage);
        })
      );
    }
  }, [page, movieState.total_pages]);
  if (!movieState.movies.length) {
    return <div>No share movies</div>;
  }
  if (initLoading) {
    return <Loading />;
  }
  if (movieState.movies.length) {
    return (
      <div>
        {movieState.movies.map((item: TMovie) => (
          <MovieElement
            key={item.id}
            video_url={item.video_url}
            description={item.description}
            title={item.title}
            shared_user_email={item.shared_user_email}
          />
        ))}
        {page < movieState.total_pages ? (
          <div>
            <button type='button' onClick={() => loadMore()}>
              Load more
            </button>
          </div>
        ) : null}
      </div>
    );
  }
  return null;
};

export default HomePage;
