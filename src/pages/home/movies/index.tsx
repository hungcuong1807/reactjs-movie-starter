import { FC } from "react";
import "./movie.css";
import getVideoId from "get-video-id";

interface IMovieElementProps {
  video_url: string;
  title?: string | null;
  description?: string | null;
  shared_user_email: string;
}
const MovieElement: FC<IMovieElementProps> = ({
  video_url,
  title,
  description,
  shared_user_email,
}) => {
  const { id } = getVideoId(video_url);
  return (
    <div className='video'>
      <div>
        <iframe
          width='400'
          height='200'
          src={`https://www.youtube.com/embed/${id}`}
          frameBorder='0'
          loading='lazy'
          allow='accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture'
          allowFullScreen
          title={title || "No title"}
        />
      </div>

      <div className='video-description'>
        <p className='title'>{`Title: ${title || "No title"}`}</p>
        <p className='description'>{`Description: ${description || "No description"}`}</p>
        <p>{`Shared by: ${shared_user_email}`}</p>
      </div>
    </div>
  );
};

export default MovieElement;
