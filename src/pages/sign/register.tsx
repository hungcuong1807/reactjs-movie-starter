import { useState, FormEvent } from "react";
import { Link, useNavigate } from "react-router-dom";
import EmailOutlinedIcon from "@mui/icons-material/EmailOutlined";
import LockOutlinedIcon from "@mui/icons-material/LockOutlined";
import "./sign.css";
import { useDispatch } from "react-redux";
import { TCreateUser, TErrorResponse } from "@API/types";
import { fetchRegister } from "../../redux/actions/creators";
import Loading from "../../components/Loading";

const Register = () => {
  const dispatch = useDispatch();
  const [inputError, setInputError] = useState("");
  const [isLoading, setLoading] = useState(false);
  const [inputSuccess, setInputSuccess] = useState("");
  const navigate = useNavigate();
  const isValidForm = (formData: TCreateUser) => {
    const { email, password } = formData;
    if (!email.length || !password.length) {
      setInputError("Please fill in all fields");
      return false;
    }

    return true;
  };
  const formSubmitHandler = async (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    const formData = new FormData(e.currentTarget) as Iterable<[TCreateUser]>;
    const entries: TCreateUser = Object.fromEntries(formData);
    // eslint-disable-next-line no-useless-return
    if (!isValidForm(entries)) return;
    setLoading(true);
    dispatch(
      fetchRegister(entries, (error?: TErrorResponse) => {
        setLoading(false);
        if (error && error.message) {
          setInputError(error.message || "Something went wrong");
        } else {
          setInputSuccess("Login successfully");
          setTimeout(() => {
            navigate("/");
          }, 200);
        }
      })
    );
  };

  return isLoading ? (
    <Loading />
  ) : (
    <div className='reg-login-page'>
      <h2>Register</h2>
      <form action='/register' onSubmit={formSubmitHandler}>
        <div className='form-item'>
          <div className='form-field with-icon'>
            <input type='email' name='email' placeholder='Email:' />
            <EmailOutlinedIcon />
          </div>
        </div>
        <div className='form-item'>
          <div className='form-field with-icon'>
            <input type='password' name='password' placeholder='Password:' />
            <LockOutlinedIcon />
          </div>
          <div className='form-error'>{inputError}</div>
          <div className='form-success'>{inputSuccess}</div>
        </div>
        <div className='form-submit'>
          <button type='submit'>Sign Up</button>
        </div>
      </form>
      <div className='change-sign-form'>
        <p>
          If you have an account? <Link to='/login'>Sign In here</Link>
        </p>
        <p>
          Back to <Link to='/'>Home</Link>
        </p>
      </div>
    </div>
  );
};

export default Register;
