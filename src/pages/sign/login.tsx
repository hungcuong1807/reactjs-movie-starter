import { useState, FormEvent } from "react";
import { Link, useNavigate } from "react-router-dom";
import PersonIcon from "@mui/icons-material/Person";
import LockOutlinedIcon from "@mui/icons-material/LockOutlined";
import "./sign.css";
import { useDispatch } from "react-redux";
import { TErrorResponse, TLoginUser } from "../../api/types";

import { fetchLoginUser } from "../../redux/actions/creators";
import Loading from "../../components/Loading";

const Login = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const [inputError, setInputError] = useState("");
  const [isLoading, setLoading] = useState(false);
  const [inputSuccess, setInputSuccess] = useState("");

  const isValidForm = (formData: TLoginUser) => {
    const { email, password } = formData;
    if (!email.length || !password.length) {
      setInputError("Please fill in all fields");
      return false;
    }

    return true;
  };
  const formSubmitHandler = async (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    const formData = new FormData(e.currentTarget) as Iterable<[TLoginUser]>;
    const entries: TLoginUser = Object.fromEntries(formData);

    // eslint-disable-next-line no-useless-return
    if (!isValidForm(entries)) return;
    setLoading(true);
    dispatch(
      fetchLoginUser(entries, (error?: TErrorResponse) => {
        setLoading(false);
        if (error && error.message) {
          setInputError(error.message || "Something went wrong");
        } else {
          setInputSuccess("Login successfully");
          setTimeout(() => {
            navigate("/");
          }, 200);
        }
      })
    );
  };

  return isLoading ? (
    <Loading />
  ) : (
    <div className='reg-login-page'>
      <h2>Login</h2>
      <form action='/login' onSubmit={formSubmitHandler}>
        <div
          className={inputError.length ? "form-item error-item" : "form-item"}
        >
          <div className='form-field with-icon'>
            <input type='email' name='email' placeholder='Email:' />
            <PersonIcon />
          </div>
          <p className='api-sign-example'>api example: kminchelle</p>
        </div>
        <div
          className={inputError.length ? "form-item error-item" : "form-item"}
        >
          <div className='form-field with-icon'>
            <input type='password' name='password' placeholder='Password:' />
            <LockOutlinedIcon />
          </div>
          <p className='api-sign-example'>api example: 0lelplR</p>
          <div className='form-error'>{inputError}</div>
          <div className='form-success'>{inputSuccess}</div>
        </div>
        <div className='form-submit'>
          <button type='submit'>Sign In</button>
        </div>
      </form>
      <div className='change-sign-form'>
        <p>
          If you haven&apos;t an account?{" "}
          <Link to='/register'>Sign Up here</Link>
        </p>
        <p>
          Back to <Link to='/'>Home</Link>
        </p>
      </div>
    </div>
  );
};

export default Login;
