import { useDispatch, useSelector } from "react-redux";
import { useEffect } from "react";

import { Tuser } from "../api/types";
import { TStateType } from "../redux/reducers";
import { fetchUserProfile } from "../redux/actions/creators";

export const useCheckAuthenticated = () => {
  const dispatch = useDispatch();
  const token = localStorage.getItem("token");
  const user: Tuser = useSelector((state: TStateType) => state.auth.user);
  useEffect(() => {
    if (token) {
      if (!user || !user.id) {
        dispatch(fetchUserProfile());
      }
    }
  }, []);
  return token ? { isAuthenticated: true } : { isAuthenticated: false };
};
