import { Link } from "react-router-dom";
// import { useCallback } from "react";

import PersonOutlineOutlinedIcon from "@mui/icons-material/PersonOutlineOutlined";
import { useDispatch, useSelector } from "react-redux";

// import { useCheckAuthenticated } from '../../hooks/useAuth';
import logo from "../../assets/images/logo.svg";
import { useCheckAuthenticated } from "../../hooks/useAuth";

import { TStateType } from "../../redux/reducers";
import { fetchLogoutUser } from "../../redux/actions/creators";

// eslint-disable-next-line react/prop-types
const Header = () => {
  const { isAuthenticated } = useCheckAuthenticated();
  const dispatch = useDispatch();
  const user = useSelector((state: TStateType) => state.auth.user);
  // const logOut = useCallback(() => dispatch(fetchLogoutUser()), []);
  return (
    <header>
      <Link to='/' className='logo'>
        <img src={logo} height='40' alt='logo' />
      </Link>
      <div className='header-nav'>
        {isAuthenticated && (
          <Link to='/'>
            <PersonOutlineOutlinedIcon />
            {user.email ? <div>{user.email}</div> : null}
          </Link>
        )}
        <Link to={!isAuthenticated ? "/login" : "/share"}>
          <button type='button'>Share</button>
        </Link>
        {!isAuthenticated ? (
          <Link to='/login'>
            <button type='button'>Login</button>
          </Link>
        ) : (
          <button
            type='button'
            onClick={() =>
              dispatch(
                fetchLogoutUser(() =>
                  setTimeout(() => {
                    window.location.href = "/";
                  }, 200)
                )
              )}
          >
            Logout
          </button>
        )}
      </div>
    </header>
  );
};

export default Header;
