# Funny movies: React-Redux-Typescript

### Project structure
- __api folder__: folder include all requests and handle axios reponse
- __assets folder__: folder contains assets such as image files, svg files
- __components folder__: include common components which are reused
- __hooks folder__: include common hooks such as auth-hook
- __pages folder__: include pages which are on browers
- __redux folder__: include actions, reducer, store 
- __test folder__: include unit-testing and e2e testing

### Project Design
I use redux-saga and MUI to establish project. In redux folder, I separated three parts: actions (dispatch action through actiontype), reducers (store app state), sagass (process saga function)

### Run project
```npm install```
```API_HOST= npm run start``` => pass API_HOST enviroment variable

### Run testing

``` npm run test```
I defined a test folder and config project to run test program with jest and typescript

